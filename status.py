from PIL import Image, ImageDraw, ImageFont, ImageFile
from io import BytesIO
import os
import requests, json 
import urllib

ImageFile.LOAD_TRUNCATED_IMAGES = True

# name of the file to save
filename = "/var/www/html/img01.png"
fnt = ImageFont.truetype('FreeMono.ttf', 11)
# create new image
image = Image.new(mode = 'RGBA', size=(252,50),color=(197,220,198,100))
draw = ImageDraw.Draw(image)
draw.text((5,3), "Aujourd'hui", font=fnt, fill=(0,0,0))
draw.text((130,3), "Demain", font=fnt, fill=(0,0,0))
# Enter your API key here 


# base_url variable to store url 
#base_url = "http://api.openweathermap.org/data/2.5/weather?"
complete_url = "https://api.openweathermap.org/data/2.5/onecall?lat=YOUR_LATlon=YOUR_LONT&appid=YOUR_API_KEY_HERE&units=metric&exclude=minutely,hourly"


# get method of requests module 
# return response object 
response = requests.get(complete_url) 

# json method of response object 
# convert json format data into 
# python format data 
x = response.json() 
daily_jason = x["daily"]
today = daily_jason[0]
tomorow = daily_jason[1]
draw.text((5,15), "Max "+str(int(today["temp"]["max"]))+"°C" , font=fnt, fill=(150,0,0))
draw.text((5,27), "Min "+str(int(today["temp"]["min"]))+"°C" , font=fnt, fill=(0,0,150))
draw.text((130,15), "Max "+str(int(tomorow["temp"]["max"]))+"°C" , font=fnt, fill=(150,0,0))
draw.text((130,27), "Min "+str(int(tomorow["temp"]["min"]))+"°C" , font=fnt, fill=(0,0,150))

complete_url = "http://openweathermap.org/img/wn/" + str(today["weather"][0]["icon"]) + "@2x.png"
urllib.request.urlretrieve(complete_url, "icon.png")
img = Image.open("icon.png")    
img = img.resize((50, 50)) 
image.paste(img, (70,0), mask=img) 

complete_url = "http://openweathermap.org/img/wn/" + str(tomorow["weather"][0]["icon"]) + "@2x.png"
urllib.request.urlretrieve(complete_url, "icon.png")
img = Image.open("icon.png")    
img = img.resize((50, 50)) 
image.paste(img, (195,0), mask=img) 

image.save(filename)
#os.system(filename)

