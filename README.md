# Cisco 7945 Meteo Status

![Phone Display](https://gitlab.com/slawomirnoury96/meteo-app-cisco-7950/-/raw/master/Screenshot)

This python script make a displayable PNG for Cisco IP Phone 7945. It's surely adaptable for other 7900 model.
It display the day and tomorrow icon and MAX MIN temperature.

It's works by cronjob, for example :

     0 * * * * python3 /var/www/html/status.py
    10 * * * * python3 /var/www/html/cgiexecute -i 192.168.100.211 http://192.168.100.8/push_meteo.php -u test -p test
    10 * * * * python3 /var/www/html/cgiexecute -i 192.168.100.210 http://192.168.100.8/push_meteo.php -u test -p test

*status.py* is called every hours to generate the png. Then 10 min later, *cgiexecute* is called to make a request to the IP Phone to load push_meteo.php. Then the phone load the .png and display it.

*cgiexecute* come from [callmanger.nz](https://usecallmanager.nz/cgi-execute-xml.html), a excellent website for developing your small app for Cisco IP Phone.
Another great source of information was [this great website](http://www.sirpnut.com/ciscoIPPhone/) that seems on the edge of becoming offline. There's is a zip of the webpage in case that happen.


Complete the python URL with your LAT,LONT and API for OpenWeather.
